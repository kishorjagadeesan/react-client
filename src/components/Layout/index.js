import PageLayout from './Layout';
import PageHeader from './Header'

export {
  PageLayout,
  PageHeader
}
