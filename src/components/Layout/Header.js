import React from 'react';
import { Layout, Dropdown, Menu } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  MenuFoldOutlined,
  UserOutlined,
  DownOutlined,
  LogoutOutlined,
  SettingOutlined,
  TagOutlined,
  BellOutlined
} from '@ant-design/icons';

import * as actionTypes from '../../store/actions/actionTypes';

const { Header } = Layout;
const href = '#';

const PageHeader = ({toggleDrawer, firstName}) => {

  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a href={href} ><UserOutlined />&nbsp;&nbsp;My Profile</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href={href}><SettingOutlined />&nbsp;&nbsp;Settings</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">
        <a href={href}><TagOutlined />&nbsp;&nbsp;Option 1</a>
      </Menu.Item>
      <Menu.Item key="4">
        <a href={href}><BellOutlined />&nbsp;&nbsp;Option 2</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="5">
        <Link to="/logout">
          <LogoutOutlined />&nbsp;&nbsp;Logout</Link>
      </Menu.Item>
    </Menu>
  );

  return (
    <Header className="site-header-layout">
      <div style={{display:'inline'}}><span className="header-menu-icon"
        onClick={toggleDrawer}><MenuFoldOutlined /></span></div>
      <div style={{display:'inline'}} className="header-user-info">
        <Dropdown overlay={menu} overlayClassName="header-menu-dropdown" trigger={['click']}>
          <a href={href} className="ant-dropdown-link" onClick={e => e.preventDefault()}>
            {firstName || 'User'} <DownOutlined />
          </a>
        </Dropdown>
      </div>
    </Header>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({type: actionTypes.TOGGLE_DRAWER}),
  }
}

const mapStateToProps = state => {
  return {
    firstName: state.auth.firstName
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PageHeader);
