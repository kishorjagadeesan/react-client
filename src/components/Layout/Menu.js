import React from 'react';
import { Menu, Drawer } from 'antd';
import { connect } from 'react-redux';
import { Link, useLocation } from "react-router-dom";
import menuItems from './menu-items';
import * as actionTypes from '../../store/actions/actionTypes';

const { SubMenu } = Menu;

const MenuLayout = ({pageTheme, menuDrawer, toggleDrawer}) => {

  let defaultSelection = 'home';
  const location = useLocation();
  let selected = location.pathname;

  const menuComponent = menuItems.items.map((item, index) => {
    if (item.link === selected){
      defaultSelection = item.id;
    }
    if (item.children){
      let subMenu = item.children.map(subMenu => {
        return <Menu.Item key={subMenu.id} icon={subMenu.icon}>
            <Link to={subMenu.link}>{subMenu.title}</Link>
          </Menu.Item>
      });
      return (
        <SubMenu key={item.id} icon={item.icon} title={item.title}>{subMenu}</SubMenu>
      )
    }else{
      return <Menu.Item key={item.id} icon={item.icon}>
        <Link to={item.link}>{item.title}</Link>
      </Menu.Item>
    }
  })

  return (
    <>
      <Menu theme={pageTheme} defaultSelectedKeys={[defaultSelection]} mode="inline">
        {menuComponent}
      </Menu>
      <Drawer visible={menuDrawer}
        className="menu-item-drawer"
        placement="left"
        closable={true}
        closeIcon={true}
        maskClosable={true}
        onClose={toggleDrawer}>
        <Menu theme={pageTheme}
          defaultSelectedKeys={[defaultSelection]}
          mode="inline"
          style={{paddingTop: '50px'}}>
          {menuComponent}
        </Menu>
      </Drawer>
    </>
  )
}

const mapStateToProps = state => {
  return {
    pageTheme: state.pageState.pageTheme,
    menuDrawer: state.pageState.menuDrawer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({type: actionTypes.TOGGLE_DRAWER})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuLayout);
