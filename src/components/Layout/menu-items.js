import React from 'react';

import {
  HomeOutlined,
  PieChartOutlined,
  BarChartOutlined,
  DesktopOutlined,
  SettingOutlined,
  UserOutlined,

} from '@ant-design/icons';

export default {
  items: [
    {
      id: 'home',
      title: 'Home',
      icon: <HomeOutlined />,
      link:"/home"
    },
    {
      id: 'dashboard',
      title: 'Dashboard',
      icon: <PieChartOutlined />,
      children: [
        {
          id: 'dashboard1',
          title: 'Dashboard1',
          icon: <BarChartOutlined />,
          link:"/"
        },
        {
          id: 'dashboard2',
          title: 'Dashboard2',
          icon: <DesktopOutlined />,
          link:"/"
        }
      ]
    },
    {
      id: 'settings',
      title: 'Settings',
      icon: <SettingOutlined />,
      link:"/"
    },
    {
      id: 'login',
      title: 'Login',
      icon: <UserOutlined />,
      link:"/login"
    },
  ]
}
