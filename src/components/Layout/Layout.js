import React from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/actionTypes';
import { Layout } from 'antd';
import MenuLayout from './Menu';

const { Sider } = Layout;

const PageLayout = ({setLayoutCollapse, collapsed, pageTheme}) => {

  const toggleCollapse = () => {
    setLayoutCollapse();
  }

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={toggleCollapse}
    theme={pageTheme}
    className="portal-side-layout"
    style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
        boxShadow:  "0 20px 0 #3f4d67"
      }}>
      <div className="portal-logo" />
      <MenuLayout/>
    </Sider>
  )
}

const mapStateToProps = state => {
  return {
    collapsed: state.pageState.collapsed,
    pageTheme: state.pageState.pageTheme,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setLayoutCollapse: () => dispatch({type: actionTypes.TOGGLE_COLLAPSE})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PageLayout);
