import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import {
  Route,
  BrowserRouter,
  Switch,
  Redirect
} from 'react-router-dom';
import { Layout } from 'antd';

import Login from './pages/Login';
import LogOut from './pages/Logout';
import Home from './pages/Home';

import { PageLayout, PageHeader } from './components/Layout';
import { refreshUserInfo } from './store/actions';
import storage from './utils/LocalStorage';

const { Content } = Layout;

const App = ({layoutCollapse, loggedIn}) => {
  useEffect( () => {
    const getUserInfo = async () => {
      // refresh user info if required.
      refreshUserInfo();
    }
    if (!loggedIn){
      getUserInfo();
    }
  }, [loggedIn])

  return (
    <>
      <BrowserRouter>
        { !storage.getToken() ? <Redirect to="/login"/> : null }
        <Switch>
          <Route path="/login">
            <Login/>
          </Route>
          <Route path="/logout">
            <LogOut/>
          </Route>
          <Route>
            <Layout style={{ minHeight: '100vh' }}>
              <PageLayout/>
              <Layout className={layoutCollapse? 'site-content-collapse' :'site-content-layout'}>
                <PageHeader/>
                <Content className="page-content-wrapper">
                  <Route path="/home" exact>
                    <Home/>
                  </Route>
                  <Route path="/" exact>
                    <Home/>
                  </Route>
                </Content>
              </Layout>
            </Layout>
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  )
}

const mapStateToProps = state => {
  return {
    layoutCollapse: state.pageState.collapsed,
    loggedIn: state.auth.loggedIn
  }
}

export default connect(mapStateToProps)(App);
