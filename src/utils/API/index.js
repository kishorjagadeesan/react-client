import axios from 'axios';
import * as siteConfig from '../../config';
import LocalStorageService from '../LocalStorage';

const API = axios.create({
  baseURL: siteConfig.default.serverURL,
  timeout: siteConfig.default.apiTimeout,
  withCredentials: true
});

const storageService = LocalStorageService.getService();

API.interceptors.request.use(
  config => {
      const token = storageService.getToken();
      if (token && config.url && config.url !== '/get-auth-tokens/') {
          config.headers['Authorization'] = 'Token ' + token;
      }
      // config.headers['Content-Type'] = 'application/json';
      return config;
  },
  error => {
      Promise.reject(error)
});

API.interceptors.response.use( response => {
  return response
},
error => {
  if (!error.response){
      return Promise.reject(error);
  }

  try{
      if (error.response.status === 401) {
          // If invalid token, then remove the token and reload the page.
          // App component will redirect to login if no token.
          storageService.clearToken();
          window.location.reload();
      }
  }catch(err){}

  return Promise.reject(error);
});

export default API;
