import {toast} from 'react-toastify';

export const responseErrorHandler = err => {
    const response = {
        status: 500,
        message: '',
        data: {},
        hasResponseError: true
    }

    try{
        if (err.response){
            response.status = err.status;
            response.data = err.response.data;
            if (err.response.data){
                response.message = err.response.data.msg ? err.response.data.msg :
                    err.response.data.detail ? err.response.data.detail : ''
            }
        }else{
            response.message = err.message
        }
    } catch (err){}

    return response;
};

const _displayToast = (status, message) => {
    try{
        if (status === 'failed' || status === 'fail' || status === 'error'){
            toast.error(message)

        } else if (status === 'success'){
            toast.success(message);

        } else if (status === 'warning'){
            toast.warning(message);

        } else if (status === 'info'){
            toast.info(message);

        }
    } catch(e){}

    return true;
}

// Display response 'message' based on the response status.
export const responseToast = (res, msgText) => {

    if (res.data){
        try{
            const status = res.data.status;
            let message = null;

            if (msgText){
                message = msgText;
            } else {
                message = res.data.message ? res.data.message
                                    : res.data.msg ? res.data.msg : null;
            }

            if (message && status){
                _displayToast(status, message)
            }
        }
        catch (e){/* Not handling error for now. */}
    }
};

export const errorResponseToast = (response, msg) => {
    // Get the cleaned response with proper error message
    const res = responseErrorHandler(response)
    let message = msg ? msg : res.message ? res.message : null;

    if (message){
        _displayToast('error', message);
    }
}
