const LocalStorageService = (function(){
  let _service;

  function _getService(){
      if (!_service){
          _service = this;
          return _service
      }
  }

  function _setToken(token){
      localStorage.setItem('access-token', token);
  }

  function _getToken(){
      return localStorage.getItem('access-token');
  }

  function _clearToken(){
      localStorage.removeItem('access-token');
  }

  function _setItem(key, val){
      localStorage.setItem(key, val);
  }

  function _getItem(key){
      localStorage.getItem(key);
  }

  function _removeItem(key){
      localStorage.removeItem(key);
  }

  return{
      getService: _getService,
      getToken: _getToken,
      setToken: _setToken,
      clearToken: _clearToken,
      setItem: _setItem,
      getItem: _getItem,
      removeItem: _removeItem
  }

})();

export default LocalStorageService;
