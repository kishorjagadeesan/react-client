import React from 'react';
import { Input, Button, Form, Checkbox } from 'antd';
import { connect } from 'react-redux';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import * as actions from '../../store/actions';
import { Redirect } from 'react-router-dom';
import LocalStorage from '../../utils/LocalStorage';

const Login = ({
  userLogin,
  loginMessage,
  loading,
  loggedIn
}) => {

  const performLogin = ({username, password, remember}) => {
    userLogin(username, password, remember||false);
  }

  if (loggedIn || LocalStorage.getToken()){
    return <Redirect to="/home" />
  }

  return (
    <div className="login-wrapper">
      <div className="login-content">
        <div className="card">
          <div className="card-body" style={{textAlign: 'center'}}>
            <div className="login-icon"><UserOutlined/></div>
            <h3 className="text-muted">Portal Login</h3>
            <Form name="site_login"
              onFinish={performLogin}>
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}>
                <Input placeholder="Username"
                  prefix={<UserOutlined className="site-form-item-icon" />}/>
              </Form.Item>
              <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!',
                    },
                  ]}>
                <Input.Password placeholder="Password"
                  prefix={<LockOutlined className="site-form-item-icon" />}/>
              </Form.Item>
              <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                  <Checkbox style={{float:'left'}}>Remember me</Checkbox>
                </Form.Item>
                <a style={{float:'right'}} href="/login">
                  Forgot password?
                </a>
              </Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                style={{width: '100%'}}
                loading={loading}>
                {loading ? 'Please Wait' :'Login'}
              </Button>
            </Form>
            <div className="error-msg">{loginMessage ? loginMessage : null}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    loginMessage: state.auth.message,
    loading: state.auth.loginState,
    loggedIn: state.auth.loggedIn
  }
}
const mapDispatchToProps = dispatch => {
  return {
    userLogin:  (username, password, remember) => dispatch(actions.siteLogin(username, password, remember))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
