import React, { useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { siteLogout } from '../../store/actions';

const Logout = ({onSiteLogout}) => {

  useEffect(() => {
    onSiteLogout();
  }, [onSiteLogout])

  return <Redirect to="/login"/>
}

const mapDispatchToProps = dispatch => {
  return {
    onSiteLogout: () => dispatch(siteLogout())
  }
}

export default connect(null, mapDispatchToProps)(Logout);
