import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import "antd/dist/antd.css";
import './styles/main.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import rootReducer from './store/store';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const app = (
  <Provider store={store}>
    <App />
  </Provider>);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
