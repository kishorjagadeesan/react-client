import { combineReducers } from 'redux';

import authReducer from './reducers/authReducer';
import pageReducer from './reducers/pageReducer';

const rootReducer = combineReducers({
  auth: authReducer,
  pageState: pageReducer
});

export default rootReducer;

