import update from "immutability-helper";
import * as actionTypes from '../actions/actionTypes';

const initialState = {
  pageName: '',
  navName: '',
  collapsed: false,
  menuDrawer: false,
  pageTheme: 'dark', // dark or light
}

const pageReducer = (state=initialState, actions) => {
  switch (actions.type){
    case actionTypes.TOGGLE_COLLAPSE:
      return update(state, {
        collapsed: {$set: !state.collapsed}
      })
    case actionTypes.TOGGLE_DRAWER:
      return update(state, {
        collapsed: {$set: state.menuDrawer ? true: false},
        menuDrawer: {$set: !state.menuDrawer}
      })
    default:
      return state;
  }
};

export default pageReducer;
