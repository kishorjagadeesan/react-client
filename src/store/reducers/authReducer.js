import update from "immutability-helper";
import * as actionTypes from '../actions/actionTypes';

const initialState = {
	username: '',
	firstName: '',
	lastName: '',
  email: '',
  message: '',
  loginState: false,
  loggedIn: false,
};

const authReducer = (state=initialState, actions) => {
  switch (actions.type){
    case actionTypes.DO_LOGIN:
      return update(state, {
        message: {$set: ''},
        loginState: {$set: true}
      })
    case actionTypes.SUCCESS_LOGIN:
      return update(state, {
        username: {$set: actions.username},
        firstName: {$set: 'Test'}, // actions.firstName,
        lastName: {$set: 'User'}, // actions.lastName,
        email: {$set: 'testuser@test.cc'}, //actions.email,
        loginState: {$set: false},
        message: {$set: 'Login Successful!!'},
        loggedIn: {$set: true}
      })
    case actionTypes.FAIL_LOGIN:
      return update(state, {
        message: {$set: 'Unable to Login'}, //actions.message,
        loginState: {$set: false},
      })
    case actionTypes.SITE_LOGOUT:
      return update(state, {
        username: {$set: ''},
        firstName: {$set: ''}, // actions.firstName,
        lastName: {$set: ''}, // actions.lastName,
        email: {$set: ''}, //actions.email,
        loginState: {$set: false},
        message: {$set: ''},
        loggedIn: {$set: false},
      })
    default:
      return state;
  }
};

export default authReducer;
