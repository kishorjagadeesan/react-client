import * as actionTypes from './actionTypes'
import storage from '../../utils/LocalStorage';
//import API from '../../utils/API';

export const doLogin = () => {
  return {type: actionTypes.DO_LOGIN}
}

const successLogin = data => {
  return {
    type: actionTypes.SUCCESS_LOGIN,
    ...data
  }
}

export const siteLogin = (username,password,remember) => {
  return dispatch => {
    dispatch(doLogin());
    //Add your login API call here.
    setTimeout(() => {
      dispatch(successLogin({ username: username, password: password, remember:remember }));
      // This is just a dummy token
      storage.setToken('sdfksfjq23kjkq23123k');
    }, 3000);
  }
}

export const siteLogout = () => {
  return dispatch => {
    dispatch({type: actionTypes.SITE_LOGOUT});
    storage.clearToken();
  }
  // delete token and other info from local storage.
}

export const refreshUserInfo = async () => {
  if (storage.getToken()){
    //get user info here
  }else{
    storage.clearToken();
  }
}
